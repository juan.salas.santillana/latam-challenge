# LATAM Airlines challenge - Juan Santillana


## Stack

- Python 3.7
- AWS Lambda
- Api Gateway
- Gitlab-CI
- Terraform

## Ramas

- master (prod)
- develop (dev)
- feature/{name} (para mantener un flujo por caracteristica)

## Acerca del Api

El api fue desarrollado usando AWS Lambda y con el Layer 'arn:aws:lambda:us-east-1:446751924810:layer:python-3-7-scikit-learn-0-23-1:2' debido a que la libreria sklearn ocupa mas de 250mb

Lambda fue elegido debido a que nos permitira escalar automaticamnte, sin preocuparnos de algoritmos de balance u otros

Luego se expone mediante aws api gateway

Se utiliza Pickle para cargar el modelo y se utiliza la funcion predict y se devuelve el resultado en json

## IaC

Se eligio Terraform debido a su estabilidad, por motivos de tiempo solo se creo un archivo main.tf en lugar de modulizar, el tfstate esta alojado en S3

## CI/CD

Como herramienta de CI/CD se utilizo Gitlab-CI debido a que estamos usando GitLab como repositorio y por su facilidad. solo se requiere definir un archivo .gitlab-ci.yml

![Pipeline](cicd.png)

El pipeline esta compuesto por 4 stages

- Compile (Comprime el api)
- Validate (Valida el main.tf)
- Plan (Imprime el terraform plan)
- Apply (Realiza los cambios en la infraestructura)

## Pruebas de stress

Para consultar el api
```
curl -X POST https://zrz5crlv8i.execute-api.us-east-1.amazonaws.com/dev -d 'eyJpbnB1dCI6WwogICAgWwogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAxLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAxLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAxLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwLAogICAgICAwCiAgICBdCiAgXX0='

```

El cual respondera
```
[0]
```

Se realizaron las pruebas de stress
```
wrk -t8 -d45 -c5000 -s config.lua https://zrz5crlv8i.execute-api.us-east-1.amazonaws.com/dev 
Running 45s test @ https://zrz5crlv8i.execute-api.us-east-1.amazonaws.com/dev
  8 threads and 5000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   233.65ms  107.69ms 692.97ms   88.47%
    Req/Sec   260.33    112.22   545.00     61.86%
  46234 requests in 45.10s, 22.92MB read
  Socket errors: connect 4758, read 0, write 0, timeout 0
  Non-2xx or 3xx responses: 41758
```

## Conclusiones

Se logro realizar la exposicion del modelo

Si bien se pudo utilizar el framework serverless el requerimiento solicitaba usar terraform

Las pruebas de stress indican un gran porcentaje de error; esto es debido al procesamiento de la maquina atacante (limitaciones de cpu, ancho de banda, hilos) al hacer pruebas en diferentes redes mientras se realizaba la prueba no mostro caidas

Lambda permitiria escalar a un mayor numero de consultas inclusive sin problema